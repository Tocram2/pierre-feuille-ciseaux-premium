package fr.dutinfo.pfcpremium;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class UserAdapter extends ArrayAdapter {
    public UserAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View myView;
        LayoutInflater myLayout = (LayoutInflater)getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
        myView = myLayout.inflate(R.layout.list_player,null);

        Users currentUser = (Users)getItem(position);
        TextView myName = (TextView)myView.findViewById(R.id.nom);
        TextView myScore = (TextView)myView.findViewById(R.id.score);

        myName.setText(currentUser.getNom());
        myScore.setText(Integer.toString(currentUser.getScore()));

        return myView;
    }
}
