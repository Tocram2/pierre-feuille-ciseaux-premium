package fr.dutinfo.pfcpremium;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;

public class MainMenuActivity extends AppCompatActivity {
    Spinner spin_difLvl;
    String difLvl;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        configureProfilButton();
        configureRulesButton();
        configureLaunchGameButton();
        configureDisconnectButton();

        spin_difLvl = (Spinner)findViewById(R.id.spin_difficulty);
        spin_difLvl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                difLvl = spin_difLvl.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //NOTHING
            }
        });

    }

    private void configureProfilButton(){
        Button btn = (Button)findViewById(R.id.btn_profil);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainMenuActivity.this, Profil_Activity.class));
            }
        });
    }
    private void configureRulesButton(){
        Button btn = (Button)findViewById(R.id.btn_rules);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(MainMenuActivity.this, Rules_Activity.class));
            }
        });
    }
    private void configureLaunchGameButton(){
        Button btn = (Button)findViewById(R.id.btn_launchGame);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainMenuActivity.this, Game_Activity.class);
                intent.putExtra("difLvl", difLvl);
                startActivity(intent);
            }
        });
    }
    private void configureDisconnectButton(){
        Button btn = (Button)findViewById(R.id.btn_deco);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainMenuActivity.this, Accueil.class);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);  // makesure user cant go back
                startActivity(intent);
            }
        });
    }
}