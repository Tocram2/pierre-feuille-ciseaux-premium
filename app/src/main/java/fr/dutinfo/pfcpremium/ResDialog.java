package fr.dutinfo.pfcpremium;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatDialogFragment;

@SuppressLint("ValidFragment")
public class ResDialog extends AppCompatDialogFragment {
    private String winner;
    private String lvl;
    @SuppressLint("ValidFragment")
    public  ResDialog(String p_winner, String p_lvl){
        this.winner = p_winner;
        this.lvl = p_lvl;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Game results")
                .setMessage("This game is over ! " + winner + " won !")
                .setPositiveButton("Main Menu", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getActivity(), MainMenuActivity.class));
                    }
                })
                .setNegativeButton("New Game", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getActivity(), Game_Activity.class);
                        intent.putExtra("difLvl", lvl);
                        startActivity(intent);
                    }
                });
        return builder.create();
    }
}
