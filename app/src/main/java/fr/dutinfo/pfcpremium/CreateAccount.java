package fr.dutinfo.pfcpremium;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class CreateAccount extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private EditText txtFirstName;
    private EditText txtLastName;
    private EditText txtUsername;
    private EditText txtEmail;
    private EditText txtPassword;
    private RadioButton rdM;
    private EditText txtAge;
    final Map<String, Object> userMap = new HashMap<>();

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            startActivity(new Intent(CreateAccount.this, AccountCreated.class));
        }
    }

    private void signIn(String email, String password) {
        if (!validateForm()) {
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            sendVerifEmail(user);
                            updateUI(user);
                        } else {
                            Toast.makeText(CreateAccount.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private void signUp(final String email, final String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    final FirebaseUser user = mAuth.getCurrentUser();

                    db.collection("users")
                            .document(user.getUid())
                            .set(userMap)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    // on se connecte au compte tout juste créé avant de continuer
                                    signIn(email, password);
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(CreateAccount.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                    updateUI(null);
                                }
                            });
                } else {
                    Toast.makeText(CreateAccount.this, "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                    updateUI(null);
                }
                }
            });
    }

    private void sendVerifEmail(final FirebaseUser user) {
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]

                        if (task.isSuccessful()) {
                            Toast.makeText(CreateAccount.this,
                                    "Verification email sent to " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CreateAccount.this,
                                    "Failed to send verification email.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [END_EXCLUDE]
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        db = FirebaseFirestore.getInstance();

        txtFirstName = findViewById(R.id.txtFirstName);
        txtLastName = findViewById(R.id.txtLastName);
        txtUsername = findViewById(R.id.txtUsername);
        txtEmail = findViewById(R.id.txtCAemail);
        txtPassword = findViewById(R.id.txtCApassword);
        rdM = findViewById(R.id.rdM);
        txtAge = findViewById(R.id.txtAge);
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = txtEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            txtEmail.setError("Needed");
            valid = false;
        } else {
            txtEmail.setError(null);
        }

        String password = txtPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            txtPassword.setError("Needed");
            valid = false;
        } else {
            txtPassword.setError(null);
        }

        String username = txtUsername.getText().toString();
        if (TextUtils.isEmpty(username)) {
            txtUsername.setError("Needed");
            valid = false;
        } else {
            txtUsername.setError(null);
        }

        return valid;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        final Button btnSubmit = findViewById(R.id.btnSubmitForm);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateForm()) {
                    return;
                }
                userMap.put("first_name", txtFirstName.getText().toString());
                userMap.put("last_name", txtLastName.getText().toString());
                userMap.put("username", txtUsername.getText().toString());
                String gender;
                if (rdM.isChecked() == true) gender = "m";
                else gender = "f";
                userMap.put("gender", gender);
                userMap.put("age", txtAge.getText().toString());
                userMap.put("score", 0);

                signUp(txtEmail.getText().toString(), txtPassword.getText().toString());
            }
        });

        final TextView txtAlreadySignedUp = findViewById(R.id.txtAlreadySignedUp);
        txtAlreadySignedUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CreateAccount.this, Accueil.class));
            }
        });
    }
}
