package fr.dutinfo.pfcpremium;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Rules_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_);

        configureBackButton();
    }

    private void configureBackButton(){
        Button btn = (Button)findViewById(R.id.btn_rulesBack);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(Rules_Activity.this, MainMenuActivity.class));
            }
        });
    }
}
