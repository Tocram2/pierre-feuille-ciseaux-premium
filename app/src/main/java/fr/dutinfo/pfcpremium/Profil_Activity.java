package fr.dutinfo.pfcpremium;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Profil_Activity extends AppCompatActivity {
    private ListView listView;
    private List<Users> myUsersList;
    private UserAdapter myUserA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_);

        final TextView txt_profilName = findViewById(R.id.txt_profilName);
        final TextView txt_scoreUser = findViewById(R.id.txt_scoreUser);
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = mAuth.getCurrentUser();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        // on récupère les infos du joueur
        DocumentReference docRef = db.collection("users").document(user.getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        txt_profilName.setText(document.getData().get("username").toString());
                        txt_scoreUser.setText(document.getData().get("score").toString());
                    } else {
                        Log.d("Profil_Activity", "No such document");
                    }
                } else {
                    Log.d("Profil_Activity", "get failed with ", task.getException());
                }
            }
        });

        myUserA = new UserAdapter(getApplicationContext(), 0);

        listView = (ListView)findViewById(R.id.lstView_leadboard);

        myUsersList = new ArrayList<>();

        // on récupère les infos de tous les joueurs
        CollectionReference colRef = db.collection("users");
        colRef.orderBy("score", Query.Direction.DESCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Log.d("Liste joueurs", "======= on commence à enregistrer les joueurs =======");
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Log.d("Liste joueurs", document.getId() + " => " + document.get("username").toString() + ", score : " + Integer.parseInt(document.get("score").toString()));
                        myUsersList.add(new Users(document.get("username").toString(), Integer.parseInt(document.get("score").toString())));
                    }
                    listView.setAdapter(myUserA);
                    myUserA.addAll(myUsersList);
                } else {
                    Log.d("Liste joueurs", "Error getting documents: ", task.getException());
                }
            }
        });

        configureBackButton();
    }

    private void configureBackButton(){
        Button btn = (Button)findViewById(R.id.btn_profilBack);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(Profil_Activity.this, MainMenuActivity.class));
            }
        });
    }
}
