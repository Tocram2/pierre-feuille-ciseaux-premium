package fr.dutinfo.pfcpremium;

import android.content.Intent;
import android.os.Debug;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Random;

public class Game_Activity extends AppCompatActivity {
    String playerChoice;
    String iaChoice;
    String strDifLvl;
    int pPoint;
    int iaPoint;
    boolean playerWon;

    /*FIREBASE RELATED*/
    final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    final FirebaseUser user = mAuth.getCurrentUser();
    final FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_);

        configureBackButton();

        /*SET DIFFICULTY LVL*/
        TextView txt_difLvl = (TextView)findViewById(R.id.txt_difLvl);
        Intent incomingData = getIntent();
        strDifLvl = incomingData.getStringExtra("difLvl");
        txt_difLvl.setText("Difficulty LVL :"+strDifLvl);

        /*BUTTONS*/
        ImageView img_lezard  = (ImageView)findViewById(R.id.img_lezard);
        ImageView img_pierre  = (ImageView)findViewById(R.id.img_pierre);
        ImageView img_feuille = (ImageView)findViewById(R.id.img_feuille);
        ImageView img_ciseaux = (ImageView)findViewById(R.id.img_ciseaux);
        ImageView img_spock   = (ImageView)findViewById(R.id.img_spock);
        /*TEXTS*/
        final TextView txt_choices = (TextView)findViewById(R.id.txt_choices);
        final TextView txt_resGame = (TextView)findViewById(R.id.txt_resGame);
        final TextView txt_pScore = (TextView)findViewById(R.id.txt_gameScorePlayer);
        final TextView txt_iaScore = (TextView)findViewById(R.id.txt_gameScoreIA);

        /*CLICK ON STRIKE CHOICE*/
        img_lezard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pPoint < 2 && iaPoint < 2){
                    playerChoice = "lezard";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                }else {
                    playerChoice = "lezard";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                    if(pPoint == 3) {
                        playerWon = true;
                        openResDialog("You");
                    }
                    else
                        if(iaPoint == 3) {
                            playerWon = false;
                            openResDialog("The AI");
                        }
                }
            }
        });
        img_pierre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pPoint < 2 && iaPoint < 2){
                    playerChoice = "pierre";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");

                } else {
                    playerChoice = "pierre";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                    if(pPoint == 3) {
                        playerWon = true;
                        openResDialog("You");
                    }
                    else
                        if(iaPoint == 3) {
                            playerWon = false;
                            openResDialog("The AI");
                        }
                }
            }
        });
        img_feuille.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pPoint < 2 && iaPoint < 2){
                    playerChoice = "feuille";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                }else {
                    playerChoice = "feuille";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                    if(pPoint == 3) {
                        playerWon = true;
                        openResDialog("You");
                    }
                    else
                        if(iaPoint == 3) {
                            playerWon = false;
                            openResDialog("The AI");
                        }
                }
            }
        });
        img_ciseaux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pPoint < 2 && iaPoint < 2){
                    playerChoice = "ciseaux";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                }else {
                    playerChoice = "ciseaux";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                    if(pPoint == 3) {
                        playerWon = true;
                        openResDialog("You");
                    }
                    else
                        if(iaPoint == 3) {
                            playerWon = false;
                            openResDialog("The AI");
                        }
                }
            }
        });
        img_spock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pPoint < 2 && iaPoint < 2){
                    playerChoice = "spock";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                }else {
                    playerChoice = "spock";
                    iaChoice = IAChoice(strDifLvl, playerChoice);
                    txt_choices.setText("Player : "+playerChoice+" IA : "+iaChoice);
                    ResRound(playerChoice, iaChoice,txt_resGame);
                    txt_pScore.setText(pPoint+"/3");
                    txt_iaScore.setText(iaPoint+"/3");
                    if(pPoint == 3) {
                        playerWon = true;
                        openResDialog("You");
                    }
                    else
                        if(iaPoint == 3) {
                            playerWon = false;
                            openResDialog("The AI");
                        }
                }
            }
        });
    }

    private void configureBackButton(){
        Button btn = (Button)findViewById(R.id.btn_gameBack);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(Game_Activity.this, MainMenuActivity.class));
            }
        });
    }

    public void openResDialog(String winner){
        if (playerWon) {
            final DocumentReference docRef = db.collection("users").document(user.getUid());
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            // on incrémente le score du joueur par le niveau (niveau 2 : score actuel + 2)
                            docRef.update("score", Integer.parseInt(document.get("score").toString()) + Integer.parseInt(strDifLvl));
                            Log.d("openResDialog", "Score : " + document.get("score") + " -> " + (Integer.parseInt(document.get("score").toString()) + Integer.parseInt(strDifLvl)));
                        }
                    }
                }
            });
        }
        ResDialog resDialog = new ResDialog(winner, strDifLvl);
        resDialog.show(getSupportFragmentManager(), "Results");
    }

    public String IAChoice(String difLvl, String pChoice){
        String iaChoice = "";
        int iaRand;

        switch (difLvl){
            case "1":
                iaRand = new Random().nextInt(100);
                iaChoice = selectChoice(pChoice, iaRand, 89);
                break;
            case "2":
                iaRand = new Random().nextInt(100);
                iaChoice = selectChoice(pChoice, iaRand, 69);
                break;
            case "3":
                iaRand = new Random().nextInt(100);
                iaChoice = selectChoice(pChoice, iaRand, 49);
                break;
            case "4":
                iaRand = new Random().nextInt(100);
                iaChoice = selectChoice(pChoice, iaRand, 29);
                break;
            case "5":
                iaRand = new Random().nextInt(100);
                iaChoice = selectChoice(pChoice, iaRand, 9);
                break;
        }
        return iaChoice;
    }

    public String selectChoice(String pChoice, int resRand, int lvl) {
        String iaChoice = "";
        if (pChoice == "lezard") {
            if (resRand <= lvl){
                iaChoice = "spock";
            }else{
                if(resRand >= (lvl - 5) && resRand <= lvl){
                    iaChoice = "lezard";
                }else {
                    iaChoice = "pierre";
                }
            }
        } else {
            if (pChoice == "pierre") {
                if (resRand <= lvl){
                    iaChoice = "ciseaux";
                }else{
                    if(resRand >= (lvl - 5) && resRand <= lvl){
                        iaChoice = "pierre";
                    }else {
                        iaChoice = "feuille";
                    }
                }
            } else {
                if (pChoice == "feuille") {
                    if (resRand <= lvl){
                        iaChoice = "spock";
                    }else{
                        if(resRand >= (lvl - 5) && resRand <= lvl){
                            iaChoice = "feuille";
                        }else {
                            iaChoice = "lezard";
                        }
                    }
                } else {
                    if (pChoice == "ciseaux") {
                        if (resRand <= lvl){
                            iaChoice = "feuille";
                        }else{
                            if(resRand >= (lvl - 5) && resRand <= lvl){
                                iaChoice = "ciseaux";
                            }else {
                                iaChoice = "pierre";
                            }
                        }
                    } else {
                        if (pChoice == "spock") {
                            if (resRand <= lvl){
                                iaChoice = "ciseaux";
                            }else{
                                if(resRand >= (lvl - 5) && resRand <= lvl){
                                    iaChoice = "spock";
                                }else {
                                    iaChoice = "feuille";
                                }
                            }
                        }
                    }
                }
            }
        }
        return iaChoice;
    }

    public void ResRound(String pChoice, String iaChoice, TextView res){
        if(pChoice == iaChoice){
            res.setText("Draw !");
        } else {
            if(pChoice == "lezard" && (iaChoice == "spock" || iaChoice == "feuille")){
                pPoint ++;res.setText("You won this round !");
            } else {
                if(pChoice == "pierre" && (iaChoice == "lezard" || iaChoice == "ciseaux")){
                    pPoint ++;res.setText("You won this round !");
                } else {
                    if (pChoice == "feuille" && (iaChoice == "pierre" || iaChoice == "spock")) {
                        pPoint++;res.setText("You won this round !");
                    } else {
                        if (pChoice == "ciseaux" && (iaChoice == "lezard" || iaChoice == "feuille")) {
                            pPoint++;res.setText("You won this round !");
                        } else {
                            if (pChoice == "spock" && (iaChoice == "pierre" || iaChoice == "ciseaux")) {
                                pPoint++;res.setText("You won this round !");
                            } else { iaPoint++; res.setText("IA won this round !");}
                        }
                    }
                }
            }
        }
    }
}
