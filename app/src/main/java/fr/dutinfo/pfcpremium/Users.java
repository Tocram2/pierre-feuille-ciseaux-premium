package fr.dutinfo.pfcpremium;

public class Users {

    private String Nom;
    private int Score;

    public Users(String nom, int score){
        this.Nom = nom;
        this.Score = score;
    }


    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }
}
